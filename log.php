<?php

$json = '{
   "version":"1.1",
   "host":"localhost",
   "short_message":"REQUEST: \/login BODY {}",
   "full_message":"REQUEST: \/login BODY {}\n",
   "timestamp":1559741040.518,
   "level":6,
   "_app_name":"backend",
   "_logger_name":"com.mostbet.integration.goldenrace.common.logging.HttpLoggingFilter",
   "_process_fingerprint":"4c5d8c09cb8c474faf2c73eaf2c5b682",
   "_thread_name":"http-nio-8070-exec-2",
   "_session_fingerprint":"ololo123"
}';

$data = json_decode(trim($json), true);


while (1) {
    $data['timestamp'] = microtime(true) + rand(0,100);
    $data['timestamp2'] = $data['timestamp'];

    $micro = sprintf("%06d",($data['timestamp'] - floor($data['timestamp'])) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $data['timestamp']) );
    $data['timestamp3'] = $d->format("Y-m-d H:i:s.u");
    file_put_contents('log/log.json', json_encode($data).  PHP_EOL, FILE_APPEND);
    sleep(1);
}
